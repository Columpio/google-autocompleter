import re
from googleapiclient.discovery import build

def readConfig(filename: str) -> str:
	with open(filename) as file:
		out = file.read().strip()
	return out

def takeFirstWord(text):
	m = re.search(r"(\w+)", text)
	return m.group(0)
def takeLastWord(text):
	return takeFirstWord(text[::-1])[::-1]

class Completer:
	def __init__(self):
		self._my_api_key = readConfig("api_key.txt")
		self._my_cse_id = readConfig("cse_id.txt")
		self._service = build("customsearch", "v1", developerKey=self._my_api_key)
		self._cse = self._service.cse()

	def _sendPureQuery(self, search_term):
		results = self._cse.list(q=search_term, cx=self._my_cse_id).execute()
		info = results['searchInformation']
		if int(info['totalResults']) == 0:
			return []
		return [x['snippet'] for x in results['items']]

	def _sendQuery(self, search_term):
		precise_results = self._sendPureQuery('"%s"' % search_term)
		inprecise_results = self._sendPureQuery(search_term)
		return precise_results + inprecise_results

	def complete(self, sentence):
		def get_next_word(sent, ctx, context):
			index = ctx.rindex(sent) + len(sent)
			return takeFirstWord(context[index:])
		def obtain_next_word(context):
			sent = sentence.lower()
			ctx = context.lower()
			if sent in ctx:
				return get_next_word(sent, ctx, context)
			last_word = takeLastWord(sent).lower()
			if last_word in ctx:
				return get_next_word(last_word, ctx, context)
			return None
		contexts = self._sendQuery(sentence)
		return filter(None, map(obtain_next_word, contexts))


c = Completer()

sent = "In the beginning"
while True:
	word = next(c.complete(sent))
	sent += " " + word
	print(sent)
	input()
